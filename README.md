Create a JSON report of contributor performance in your repository.

## Install dependencies

    npm i
   
## Build

    npm run build

## Install globally
    
    npm install -g .
    
## Usage

    cd path/to/git/repository
    cope
    
## Uninstall globally

    npm uninstall -g cope
