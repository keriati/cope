import * as util from "util";
import { exec } from "child_process";
import { Executor } from "./Executor";

export class ShellExecutor implements Executor {
  async run(command: string): Promise<string> {
    const { stdout, stderr } = await util.promisify(exec)(command);
    return stdout;
  }
}
