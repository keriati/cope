export interface Executor {
  run(command: string): Promise<string>;
}
