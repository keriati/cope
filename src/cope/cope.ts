#!/usr/bin/env node
import { GitRepository } from "./GitRepository";
import { ShellExecutor } from "./ShellExecutor";
import { Stats } from "./Stats";

const JSON_OUT_INDENT = 2;

async function main() {
  console.error('Welcome to COntributor PErformance!');

  const since = new Date("2019");
  const until = new Date("2020");

  console.error(`Using data from ${since.toISOString()}`);
  console.error(`Using data until ${until.toISOString()}`);

  console.error('Finding contributors...');

  const myExecutor = new ShellExecutor();
  const repo = new GitRepository(myExecutor);
  const authors = await repo.getAuthors(since, until);

  console.error(`Found ${authors.length} contributors.`);
  const results = [];

  for (let author of authors) {
    console.error(`${authors.indexOf(author) + 1}/${authors.length} Loading statistics for ${author.toString()}...`);
    const authorStats = await repo.getAuthorStats(author, since, until);

    results.push({ author, ...authorStats });
  }

  const cleanResults = Stats.getWithoutEmpty(results);

  console.log(JSON.stringify(cleanResults, null, JSON_OUT_INDENT));
}

main();
