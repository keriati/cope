export class Stats {
  static getWithoutEmpty(results: Array<object>) {
    return results.filter(Stats.hasAdded);
  }

  static hasAdded(result) {
    if (!result.added || typeof result.added !== "number") {
      return false;
    }

    return result.added > 0;
  }
}
