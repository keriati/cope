import { Author } from "../Author";

describe("Author", () => {
  it("is created with name and email", () => {
    const email = "foo@bar.com";
    const name = "Foo";

    const author = new Author(name, email);

    expect(author.email).toBe(email);
    expect(author.name).toBe(name);
  });

  describe("#toString()", function() {
    it("returns name with email address format", () => {
      const email = "foo@bar.com";
      const name = "Foo";

      const author = new Author(name, email);

      expect(author.toString()).toBe(`${name} <${email}>`);
    });
  });
  describe("#createFromString()", function() {
    it("return Author object with name and email set from json string", () => {
      const email = "foo@bar.com";
      const name = "Foo";
      const JSONAuthor = JSON.stringify({name, email});

      const author = Author.createFromString(JSONAuthor);

      expect(author.email).toBe(email);
      expect(author.name).toBe(name);
    });
  });
});
