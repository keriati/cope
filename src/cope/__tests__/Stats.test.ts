import { Stats } from "../Stats";

describe("Stats", () => {
  describe("#getWithoutEmpty()", function() {
    it("returns a new array with statistics with 0 added lines removed", function() {
      const stats = [
        {
          author: {
            name: "foo1",
            email: "foo@bar.com"
          },
          added: 0,
          deleted: 0
        },
        {
          author: {
            name: "foo2",
            email: "foo2@bar.com"
          },
          added: 1,
          deleted: 0
        }
      ];

      const actual = Stats.getWithoutEmpty(stats);

      // @ts-ignore
      expect(actual[0]?.author?.name).toBe("foo2");
      expect(actual).toHaveLength(1);
    });
  });
});
