import { GitRepository } from "../GitRepository";
import { Author } from "../Author";

describe("GitRepository", () => {
  describe('#getAuthorsCommand()', () => {
    it('returns the command to get all authors', () => {
      const since = new Date("2019");
      const until = new Date("2020");

      const actual = GitRepository.getAuthorsCommand(since, until);

      expect(actual).toBe(`git log --format='{\"name\":\"%aN\",\"email\":\"%ae\"}' --since=\"2019-01-01T00:00:00.000Z\" --until=\"2020-01-01T00:00:00.000Z\" | sort -u`);
    });
  });
  describe("getAuthorStatsCommand", () => {
    it("returns the author stats command with name and email prefilled", () => {
      const author = new Author("a b", "a@b.c");

      const command = GitRepository.getAuthorStatsCommand(author);

      expect(command).toBe(
        `git log --shortstat --author="a b <a@b.c>" | grep -E "fil(e|es) changed" | awk '{added+=$4; deleted+=$6} END {print "{\\\"added\\\":",added,",\\\"deleted\\\":",deleted,"}"}'`
      );
    });
    it("returns the author stats command with since date pre-filled", function() {
      const author = new Author("a b", "a@b.c");
      const since = new Date("2018");

      const command = GitRepository.getAuthorStatsCommand(author, since);

      expect(command).toBe(
        `git log --shortstat --author="a b <a@b.c>" --since="2018-01-01T00:00:00.000Z" | grep -E "fil(e|es) changed" | awk '{added+=$4; deleted+=$6} END {print "{\\\"added\\\":",added,",\\\"deleted\\\":",deleted,"}"}'`
      );
    });
    it("returns the author stats command with until date pre-filled", function() {
      const author = new Author("a b", "a@b.c");
      const until = new Date("2018");

      const command = GitRepository.getAuthorStatsCommand(
        author,
        undefined,
        until
      );

      expect(command).toBe(
          `git log --shortstat --author="a b <a@b.c>" --until="2018-01-01T00:00:00.000Z" | grep -E "fil(e|es) changed" | awk '{added+=$4; deleted+=$6} END {print "{\\\"added\\\":",added,",\\\"deleted\\\":",deleted,"}"}'`
      );
    });
  });
  describe("mapAuthorStringArrayToAuthor", function() {
    it("returns an array of Author objects when passed an array of valid stringified json objects", function() {
      const authorStringArray = ['{"name": "a b", "email": "a@b.c"}'];

      const authors = GitRepository.mapAuthorStringArrayToAuthor(
        authorStringArray
      );

      expect(authors[0]).toBeInstanceOf(Author);
      expect(authors[0].name).toBe("a b");
      expect(authors[0].email).toBe("a@b.c");
    });
  });
});
