export class Author {
  constructor(
      public readonly name: string,
      public readonly email: string
  ) {}

  toString(): string {
    return `${this.name} <${this.email}>`;
  }

  static createFromString(author: string): Author {
    const authorObject = JSON.parse(author);
    return new Author(authorObject.name, authorObject.email);
  }
}
