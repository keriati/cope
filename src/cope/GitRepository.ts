import { Executor } from "./Executor";
import { Author } from "./Author";

const GIT_AUTHORS_COMMAND = `git log --format='{"name":"%aN","email":"%ae"}'`;
const SORT_COMMAND = `sort -u`;
const GIT_SHORTSTATS = `git log --shortstat`;
const GIT_AUTHOR_PARAM = `--author`;
const GIT_SINCE_PARAM = `--since`;
const GIT_UNTIL_PARAM = `--until`;
const SHORTSTATS_FORMATING = `grep -E "fil(e|es) changed" | awk '{added+=$4; deleted+=$6} END {print "{\\"added\\":",added,",\\"deleted\\":",deleted,"}"}'`;

export class GitRepository {
  private executor: Executor;

  constructor(executor: Executor) {
    this.executor = executor;
  }

  async getAuthors(since?: Date, until?: Date): Promise<Array<Author>> {
    const commandResult = await this.executor.run(GitRepository.getAuthorsCommand(since, until));

    const authors = commandResult.trim().split("\n");
    return GitRepository.mapAuthorStringArrayToAuthor(authors);
  }

  static getAuthorsCommand(since?: Date, until?: Date) {
    let command = GIT_AUTHORS_COMMAND;

    if (since) {
      command = `${command} ${GIT_SINCE_PARAM}="${since.toISOString()}"`;
    }

    if (until) {
      command = `${command} ${GIT_UNTIL_PARAM}="${until.toISOString()}"`;
    }

    return `${command} | ${SORT_COMMAND}`;
  }

  static mapAuthorStringArrayToAuthor(author: Array<string>): Array<Author> {
    return author.map(Author.createFromString);
  }

  async getAuthorStats(
    author: Author,
    since?: Date,
    until?: Date
  ): Promise<Object> {
    const command = GitRepository.getAuthorStatsCommand(author, since, until);
    const result = await this.executor.run(command);

    try {
      return JSON.parse(result);
    } catch (e) {
      return this.getZeroStats();
    }
  }

  static getAuthorStatsCommand(author: Author, since?: Date, until?: Date) {
    let command = `${GIT_SHORTSTATS} ${GIT_AUTHOR_PARAM}="${author}"`;

    if (since) {
      command = `${command} ${GIT_SINCE_PARAM}="${since.toISOString()}"`;
    }

    if (until) {
      command = `${command} ${GIT_UNTIL_PARAM}="${until.toISOString()}"`;
    }

    return `${command} | ${SHORTSTATS_FORMATING}`;
  }

  private getZeroStats() {
    return {
      added: 0,
      deleted: 0
    };
  }
}
